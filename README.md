Box.com API integration
=======================

This module exposes the Box API to Drupal, and provides functionality other modules may leverage, such as authorizing Box Apps, and handling saving/refreshing of access tokens.


### Dependencies:

* Entity: https://www.drupal.org/project/entity
* Composer manager: https://drupal.org/project/composer_manager


### Installation:

1. Add the following line in `settings.php` to be able to authorize a Box App:
     `$conf['https'] = TRUE;`
2. Download module, ensuring dependencies.
3. To enable: This module requires `composer_manager` to install it's dependencies, so please follow detailed instructions from their page:
https://github.com/cpliakas/composer-manager-docs/blob/master/README.md#maintaining-dependencies
  
If you are already familiar with the process, you can just enable using *drush*, to ensure *composer* can auto-generate class loader and install dependencies: `drush en box_api`.

### Notes:

* Please note that other modules will probably need a Box Application Client ID and Secret.

### Overview for site builders

This is an API module, so it doesn't provide any end-user features.
