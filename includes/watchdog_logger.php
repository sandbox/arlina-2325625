<?php

use Guzzle\Log;


/**
 * Adapts a Watchdog logger object for guzzle requests
 */
class BoxAPIWatchdogLogAdapter implements Guzzle\Log\LogAdapterInterface {

  /**
   * Log messages to watchdog.
   */
  public function log($message, $priority = LOG_INFO, $extras = NULL) {
    watchdog('box_api_request', $message, array(), $priority);
  }
}
